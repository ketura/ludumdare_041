﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(WaitToReturnToMainMenu());
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKey(KeyCode.Escape))
		{
			SceneManager.LoadScene("MainMenu");
		}
		
	}

	IEnumerator WaitToReturnToMainMenu()
	{
		yield return new WaitForSeconds(10);

		SceneManager.LoadScene("MainMenu");
	}

}
