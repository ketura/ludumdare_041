﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : MonoBehaviour
{
	public Animation SFDoor;

	public bool PlayOpen;
	private bool last;
	
	// Use this for initialization
	void Start ()
	{
		SFDoor.animatePhysics = true;
		SFDoor.cullingType = AnimationCullingType.AlwaysAnimate;
		SFDoor.playAutomatically = false;

		SFDoor.AddClip(AnimationManager.Instance.DoorClose, "close");
		SFDoor.AddClip(AnimationManager.Instance.DoorOpen, "open");
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.F9))
			PlayOpen = !PlayOpen;

		if(PlayOpen != last)
		{
			if (PlayOpen)
				Open();
			else
				Close();

			last = PlayOpen;
		}
	}

	public void Open()
	{
		SFDoor.Stop();

		SFDoor.Play("open");
	}

	public void Close()
	{
		SFDoor.Stop();

		SFDoor.Play("close");
	}
}
