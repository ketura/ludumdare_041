﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageTrigger : MonoBehaviour
{

	public bool MultipleTrigger;
	public bool TriggeredOnce;

	public MessageInfo MessageInfo;


	private void OnTriggerEnter(Collider other)
	{
		PlayerController pc = other.gameObject.GetComponent<PlayerController>();

		if (pc == null)
			return;

		if(TriggeredOnce && !MultipleTrigger)
		{
			return;
		}

		TriggeredOnce = true;

		MessageInfo.SayMessage();

		


	}
}
