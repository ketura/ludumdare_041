﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pedestal : MonoBehaviour
{
	public GameObject NotReadyPedestal;
	public GameObject ReadyPedestal;
	public GameObject PressedPedestal;

	public GameObject CurrentPedestal;

	public GameObject Player;

	public bool Active;
	public bool Pressed;

	public float PressDistance = 2.0f;

	public DoorOpen[] DoorsToOpen;
	public Spawner[] SpawnsToActivate;

	public MessageInfo MessageToTrigger;

	private bool wasActive;
	private bool wasPressed;

    [SerializeField]
    protected AudioClip audioPress;

    [SerializeField]
    protected AudioClip audioDoorOpen;

	// Use this for initialization
	void Start ()
	{
		wasActive = false;
		wasPressed = false;

		Player = GameObject.FindGameObjectWithTag("Player");

		SyncModel();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(wasActive != Active || wasPressed != Pressed)
		{
			SyncModel();
		}

		if (Active && !Pressed && Input.GetKey(KeyCode.F))
		{
			if(Vector3.Distance(transform.position, Player.transform.position) < PressDistance)
			{
				Pressed = true;

				foreach (var door in DoorsToOpen)
				{
					door.Open();
				}

				foreach (var spawn in SpawnsToActivate)
				{
					spawn.Spawn();
				}

				if(MessageToTrigger != null)
				{
					MessageToTrigger.SayMessage();
				}

                AudioManager.Instance.PlayClip(audioPress);
                AudioManager.Instance.PlayClip(audioDoorOpen);
            }
        }
	}

	public void SyncModel()
	{
		GameObject.Destroy(CurrentPedestal);

		if (Active)
		{
			wasActive = true;

			if (Pressed)
			{
				wasPressed = true;
				CurrentPedestal = Instantiate(PressedPedestal, this.transform, false);
			}
			else //Active
			{
				wasPressed = false;
				CurrentPedestal = Instantiate(ReadyPedestal, this.transform, false);
			}
		}
		else //Inactive
		{
			wasActive = false;
			CurrentPedestal = Instantiate(NotReadyPedestal, this.transform, false);
		}
	}


}
