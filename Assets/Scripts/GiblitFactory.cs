﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiblitFactory : MonoBehaviour
{
    [SerializeField]
    GameObject giblitPrefab;

    public void CreateGiblits(GameObject rootOfDyingUnit)
    {
        Transform rootTransform = rootOfDyingUnit.transform;
        Transform modelTransform = rootTransform.Find("Model");

        if (modelTransform != null)
        {
            foreach (Transform model in modelTransform)
            {
                GameObject part = model.gameObject;
                GameObject giblit = Instantiate(giblitPrefab);
                giblit.GetComponent<Giblit>().SetObjectToMimic(part);
            }
        }
    }
}
