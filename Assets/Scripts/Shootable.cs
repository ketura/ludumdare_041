﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shootable : MonoBehaviour
{
    [SerializeField]
    public int hp = 100;

    private int hpMax;

    [SerializeField]
    public bool invincible = false;

    [SerializeField]
    public bool regenerates = false;

    [SerializeField]
    public float regenerationTime = 5f;

    [SerializeField]
    public int healRate = 0;

    private float healTimer = 0;

    [SerializeField]
    public bool disappearWhileDead = false;

    private bool dead = false;
    private float regenerationTimer = 0f;

    private Transform modelTransform;

    private Dictionary<Transform, Color> colorMap;

    private float hitFlashTimer = 0f;

    [SerializeField]
    protected AudioClip audioDamage;

    [SerializeField]
    protected AudioClip audioDeath;

    [SerializeField]
    protected AudioClip audioRevive;

    public void Start()
    {
        hpMax = hp;
        modelTransform = LD41Utils.FindDeepChildWithName(transform, "Model");

        if (modelTransform != null)
        {
            colorMap = new Dictionary<Transform, Color>();
            foreach (Transform child in modelTransform)
            {
                colorMap.Add(child, child.GetComponent<Renderer>().material.color);
            }
        }
    }

    public void Update()
    {
        if (dead)
        {
            regenerationTimer -= Time.deltaTime;

            if (regenerationTimer <= 0)
            {
                AudioManager.Instance.PlayClip(audioRevive);
                SetDead(false);
            }
        }
        else if (healRate > 0 && hp < hpMax)
        {
            healTimer += Time.deltaTime;

            if (healTimer > 1f)
            {
                healTimer -= 1f;

                hp += healRate;
                if (hp > healRate) hp = healRate;
            }
        }

        if (hitFlashTimer > 0)
        {
            hitFlashTimer -= Time.deltaTime;

            if (hitFlashTimer <= 0)
            {
                RevertModelColor();
            }
        }
    }

    public void TakeDamage(int damage)
    {
        if (dead) return;

        AudioManager.Instance.PlayClip(audioDamage);

        if (invincible) return;

        hp -= damage;
        if (hp <= 0)
        {
            Die();
        }
        else if (!disappearWhileDead) 
        {
            SetModelColor(Color.red);
            hitFlashTimer = 0.05f;
        }
    }

    public void Die()
    {
        AudioManager.Instance.PlayClip(audioDeath);

        if (regenerates)
        {
            SetDead(true);
            return;
        }

        GiblitFactory giblitFactory = GetComponent<GiblitFactory>();
        if (giblitFactory != null) giblitFactory.CreateGiblits(gameObject);

        Destroy(transform.gameObject);
    }

    public bool IsDead()
    {
        return dead;
    }

    public void SetDead(bool isDead)
    {
        dead = isDead;

        if (dead)
        {
            regenerationTimer = regenerationTime;

            if (disappearWhileDead)
            {
                Renderer renderer = transform.GetComponent<Renderer>();
                if (renderer != null) renderer.enabled = false;

                Collider collider = transform.GetComponent<Collider>();
                if (collider != null) collider.enabled = false;
            }
            else
            {
                SetModelColor(Color.gray);
            }
        }
        else
        {
            hp = hpMax;

            if (disappearWhileDead)
            {
                Renderer renderer = transform.GetComponent<Renderer>();
                if (renderer != null) renderer.enabled = true;

                Collider collider = transform.GetComponent<Collider>();
                if (collider != null) collider.enabled = true;
            }
            else
            {
                RevertModelColor();
            }
        }
    }

    private void SetModelColor(Color color)
    {
        if (modelTransform == null) return;

        foreach (Transform child in modelTransform)
        {
            child.GetComponent<Renderer>().material.color = color;
        }
    }

    private void RevertModelColor()
    {
        if (modelTransform == null) return;

        foreach (Transform child in modelTransform)
        {
            Color originalColor;
            bool rememberedColor = colorMap.TryGetValue(child, out originalColor);
            if (rememberedColor)
            {
                child.GetComponent<Renderer>().material.color = originalColor;
            }
            else
            {
                child.GetComponent<Renderer>().material.color = Color.white;
            }
        }
    }
}
