﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class GameHandler : MonoBehaviour {

	public Shootable Player;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log("Not Dead");
		if (Player.hp <= 0)
		{
			Debug.Log("Dead");
			SceneManager.LoadScene("Game_Over");
		}
		
	}
}
