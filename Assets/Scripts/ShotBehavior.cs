﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotBehavior : MonoBehaviour
{
    public int damage = 20;
    public float trailTimer = 0.10f;
    public float trailTimerVariance = 0.02f;
    public float trailPositionVariance = 0.05f;
    public GameObject trailObject;

    public GameObject sparkObject;
    public float sparkPositionVariance = 0.15f;

    private HashSet<string> interactTags;

    private float timer;

    private GameObject originalShooter;

    private readonly string SOLIDTAG = "Solid";

    [SerializeField]
    protected AudioClip audioHitWall;

    public void Awake()
    {
        interactTags = new HashSet<string>();
    }

    // Use this for initialization
    public void Start ()
    {       
        timer = trailTimer;
	}
	
	// Update is called once per frame
	public void Update ()
    {
        if (trailObject != null)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = trailTimer + Random.Range(-trailTimerVariance, trailTimerVariance);
                GameObject trail = Instantiate(trailObject);

                Vector3 trailPosition = transform.position;
                trailPosition.x += Random.Range(-trailPositionVariance, trailPositionVariance);
                trailPosition.y += Random.Range(-trailPositionVariance, trailPositionVariance);
                trailPosition.z += Random.Range(-trailPositionVariance, trailPositionVariance);
                trail.transform.position = trailPosition;
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        bool hitSomething = false;
        if (other.tag == SOLIDTAG)
        { 
            hitSomething = true;
            AudioManager.Instance.PlayClip(audioHitWall);
        }

        GameObject root = LD41Utils.GetGameObjectRoot(other.transform);

        if (originalShooter == root) return;

        Shootable shootable = other.transform.GetComponent<Shootable>();

        if (shootable == null)
        {
            shootable = root.GetComponent<Shootable>();
        }

        if (shootable != null && (!shootable.IsDead() || !shootable.disappearWhileDead))
        {
            hitSomething = true;

            if (interactTags.Contains(root.tag))
            {
                shootable.TakeDamage(damage);
            }
        }

        if (hitSomething)
        {
            int numberOfSparks = Random.Range(3, 5);
            for (int i = 0; i < numberOfSparks; i++)
            {
                GameObject spark = Instantiate(sparkObject);
                Vector3 sparkPosition = transform.position;
                sparkPosition.x += Random.Range(-sparkPositionVariance, sparkPositionVariance);
                sparkPosition.y += Random.Range(-sparkPositionVariance, sparkPositionVariance);
                sparkPosition.z += Random.Range(-sparkPositionVariance, trailPositionVariance);
                spark.transform.position = sparkPosition;
            }

            Destroy(transform.gameObject);
        }
    }

    public void AddInteractTag(string tagString)
    {
        interactTags.Add(tagString);
    }

    public void AddOriginalShooter(GameObject originalShooterRoot)
    {

    }
}
