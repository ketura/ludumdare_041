﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

	public GameObject SpherePrefab;
	public GameObject TrapPrefab;
	public GameObject PyramidPrefab;

	public Transform[] SphereSpawns;
	public Transform[] TrapSpawns;
	public Transform[] PyramidSpawns;

	public bool Spawned;
	private bool wasSpawned;


	// Use this for initialization
	void Start ()
	{
		Spawned = false;
		wasSpawned = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Spawned && !wasSpawned)
		{
			Spawn();
		}
	}

	public void Spawn()
	{
		foreach (Transform t in SphereSpawns)
		{
			Instantiate(SpherePrefab, t.position, Quaternion.identity);
		}

		foreach (Transform t in TrapSpawns)
		{
			Instantiate(TrapPrefab, t.position, Quaternion.identity);
		}

		foreach (Transform t in PyramidSpawns)
		{
			Instantiate(PyramidPrefab, t.position, Quaternion.identity);
		}
	}
}
