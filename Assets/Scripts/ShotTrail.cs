﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotTrail : MonoBehaviour
{
    public float lifetime = 0.5f;
    public float velocity = 0f;
    private float timer;
    private Vector3 originalScale;
    private Vector3 stepTranslation;

	void Start ()
    {
        timer = lifetime;

        Vector3 localScale = transform.localScale;
        originalScale = new Vector3(localScale.x, localScale.y, localScale.z);

        stepTranslation = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
        stepTranslation.Normalize();
        stepTranslation *= velocity;
	}

	void Update ()
    {       
        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            Destroy(transform.gameObject);
        }
        else
        {
            float scale = timer / lifetime;
            transform.localScale = new Vector3(originalScale.x * scale, originalScale.y * scale, originalScale.x * scale);

            transform.Translate(stepTranslation * Time.deltaTime);
        }
	}
}
