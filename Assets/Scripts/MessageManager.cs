﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Utilities;

public class MessageManager : Singleton<MessageManager>
{

	public Text Output;

	public string DefaultText;

	public void Awake()
	{
		SetText(DefaultText);
	}

	public void SetText(string text)
	{
		Output.text = text;
	}

}
