﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageInfo : MonoBehaviour
{
	private MessageManager MessageManager;

	public string Message;
	// Use this for initialization
	void Start ()
	{
		MessageManager = MessageManager.Instance;
	}
	
	

	public void SayMessage()
	{
		MessageManager.SetText(Message);
	}
}
