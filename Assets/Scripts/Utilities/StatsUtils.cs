﻿using UnityEngine;

class StatsUtils
{
    private static double[] zValues = new double[]
    {
        -1.64485, -1.55477, -1.47579, -1.40507, -1.34076, -1.28155, -1.22653,
        -1.17499, -1.12639, -1.08032, -1.03643, -0.99446, -0.95417, -0.91537,
        -0.87790, -0.84162, -0.80642, -0.77219, -0.73885, -0.70630, -0.67449,
        -0.64335, -0.61281, -0.58284, -0.55338, -0.52440, -0.49585, -0.46770,
        -0.43991, -0.41246, -0.38532, -0.35846, -0.33185, -0.30548, -0.27932,
        -0.25335, -0.22754, -0.20189, -0.17637, -0.15097, -0.12566, -0.10043,
        -0.07527, -0.05015, -0.02507, -0.00000,  0.02507,  0.05015,  0.07527,
         0.10043,  0.12566,  0.15097,  0.17637,  0.20189,  0.22754,  0.25335,
         0.27932,  0.30548,  0.33185,  0.35846,  0.38532,  0.41246,  0.43991,
         0.46770,  0.49585,  0.52440,  0.55338,  0.58284,  0.61281,  0.64335,
         0.67449,  0.70630,  0.73885,  0.77219,  0.80642,  0.84162,  0.87790,
         0.91537,  0.95417,  0.99446,  1.03643,  1.08032,  1.12639,  1.17499,
         1.22653,  1.28155,  1.34076,  1.40507,  1.47579,  1.55477,  1.64485
    };

    public static double RandomZ()
    {
        return zValues[Random.Range(0, zValues.Length - 1)];
    }

    public static double RandomX(double mean, double stddev)
    {
        return mean + RandomZ() * stddev;
    }
}
