﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LookMode
{
	None,
	FirstPerson,
	ThirdPerson,
	Both
}

public class PlayerController : MonoBehaviour
{
	public bool ActiveControls;

	public MouseLook MouseLookController;
	public MovementController MovementController;

	public Shootable RobotGun;

	public GameObject BulletPrefab;
	public Transform BulletSpawn;
	public float BulletSpeed = 10.0f;
	public float Cooldown = 1.0f;

	public LookMode ActiveMover;
	public LookMode ActiveLooker;

    [SerializeField]
    protected AudioClip audioFireGun;

	// Use this for initialization
	void Start ()
	{
		ActiveMover = LookMode.FirstPerson;
		ActiveMover = LookMode.FirstPerson;

		ActiveControls = true;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			ActiveControls = !ActiveControls;
		}

		if (Input.GetMouseButton(0))
		{
			ActiveMover = LookMode.ThirdPerson;
		}
		else
		{
			ActiveMover = LookMode.FirstPerson;
		}

		if(Input.GetKey(KeyCode.LeftShift))
		{
			ActiveMover = LookMode.Both;
		}

		if (Input.GetMouseButton(1))
		{
			ActiveLooker = LookMode.ThirdPerson;
		}
		else
		{
			ActiveLooker = LookMode.FirstPerson;
		}

		if (ActiveControls)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;

			MouseLookController.ChangeInput(ActiveLooker);
			MovementController.LookMode = ActiveMover;
		}
		else
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;

			MouseLookController.LookMode = LookMode.None;
			MovementController.LookMode = LookMode.None;
		}

		if(Input.GetKey(KeyCode.Space) && !RobotGun.IsDead())
		{
			StartCoroutine(Shoot());

		}
	}

	private bool shooting = false;

	IEnumerator Shoot()
	{
		if(!shooting)
		{
			shooting = true;

			Ray ray = new Ray(BulletSpawn.position, MouseLookController.Gun.transform.forward);
			var shot = Instantiate(BulletPrefab, BulletSpawn.position, MouseLookController.Turret.transform.localRotation) as GameObject;
			shot.GetComponent<Rigidbody>().velocity = ray.direction.normalized * BulletSpeed;
            shot.GetComponent<ShotBehavior>().AddInteractTag("Enemy");

            AudioManager.Instance.PlayClip(audioFireGun);

            yield return new WaitForSeconds(Cooldown);


			shooting = false;
		}
	}
}
