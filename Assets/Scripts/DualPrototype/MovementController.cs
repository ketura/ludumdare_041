﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
	public float PlayerSpeed = 10.0f;
	public float RobotSpeed = 3.0f;

	public GameObject Player;
	public GameObject Robot;

	public LookMode LookMode;

	public float YThreshold = 5.0f;

    [SerializeField]
    protected AudioClip audioRobotMovement;

    private bool audioRunning = false;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		float translation = Input.GetAxis("Vertical") * Time.deltaTime;
		float strafe = Input.GetAxis("Horizontal") * Time.deltaTime;

		if (LookMode == LookMode.FirstPerson )
		{
			Player.transform.Translate(strafe * PlayerSpeed, 0, translation * PlayerSpeed);
		}

		if (LookMode == LookMode.ThirdPerson)
		{
			Robot.transform.Translate(strafe * RobotSpeed, 0, translation * RobotSpeed);
            StartCoroutine("PlayRobotMovementAudio");
        }

		if (LookMode == LookMode.Both)
		{
			Player.transform.Translate(strafe * RobotSpeed, 0, translation * RobotSpeed);
			Robot.transform.Translate(strafe * RobotSpeed, 0, translation * RobotSpeed, Player.transform);
            StartCoroutine("PlayRobotMovementAudio");
        }


	}

    IEnumerator PlayRobotMovementAudio()
    {
        if (!audioRunning)
        {
            audioRunning = true;
            AudioManager.Instance.PlayClip(audioRobotMovement);
            yield return new WaitForSeconds(0.40f);
            audioRunning = false;
        }
    }
}
