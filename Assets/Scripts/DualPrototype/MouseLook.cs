﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class MouseLook : MonoBehaviour
{
	private Vector2 smoothV;

	private Vector2 lastPlayerMouseLook;
	private Vector2 lastRobotMouseLook;

	private Vector2 Offset;

	public GameObject Player;
	public Camera PlayerCamera;

	public GameObject Turret;
	public GameObject Gun;


	public float sensitivity = 5.0f;
	public float smoothing = 2.0f;

	public LookMode LookMode;

	// Use this for initialization
	void Start ()
	{
		Offset = new Vector2();
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (LookMode == LookMode.FirstPerson || LookMode == LookMode.Both)
		{
			FacePlayer();
			PlayerCamera.transform.localRotation = Quaternion.AngleAxis(-lastPlayerMouseLook.y, Vector3.right);
			Player.transform.localRotation = Quaternion.AngleAxis(lastPlayerMouseLook.x, Player.transform.up);
			//Treads.transform.rotation = Quaternion.AngleAxis(lastPlayerMouseLook.x, Player.transform.up);
		}

		if (LookMode == LookMode.ThirdPerson || LookMode == LookMode.Both)
		{
			FaceRobot();
			Gun.transform.localRotation = Quaternion.AngleAxis(-lastRobotMouseLook.y, Vector3.right);
			Turret.transform.rotation = Quaternion.AngleAxis(lastRobotMouseLook.x, Vector3.up);
		}
		
	}

	private void FaceRobot()
	{
		lastRobotMouseLook = ProcessLook(lastRobotMouseLook, -10, 30);
	}

	private void FacePlayer()
	{
		lastPlayerMouseLook = ProcessLook(lastPlayerMouseLook, -90, 90);
	}

	private Vector2 ProcessLook(Vector2 previous, float min, float max)
	{
		var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
		md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));

		smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1.0f / smoothing);
		smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1.0f / smoothing);

		var mouseLook = previous;
		mouseLook += smoothV;

		if (mouseLook.y < min)
		{
			mouseLook.y = min;
		}

		if (mouseLook.y > max)
		{
			mouseLook.y = max;
		}

		return mouseLook;
	}

	public void ChangeInput(LookMode mode)
	{
		if (LookMode == mode)
			return;

		//var lastML = mouseLook;

		//switch (LookMode)
		//{
		//	case LookMode.FirstPerson:
		//		lastPlayerMouseLook = mouseLook;
		//		break;
		//	case LookMode.ThirdPerson:
		//		lastRobotMouseLook = mouseLook;
		//		break;
		//	case LookMode.Both:
		//		lastPlayerMouseLook = mouseLook;
		//		lastRobotMouseLook = mouseLook;
		//		break;
		//}

		//switch (mode)
		//{
		//	case LookMode.FirstPerson:
		//		mouseLook = lastPlayerMouseLook;
		//		break;
		//	case LookMode.ThirdPerson:
		//		mouseLook = lastRobotMouseLook;
		//		break;
		//}

		LookMode = mode;
	}

}
