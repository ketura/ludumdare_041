﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Giblit : MonoBehaviour
{
    [SerializeField]
    public GameObject smokeObject;

    private GameObject gameObjectToMimic;

    public float Lifetime { get; set; } = 4f;
    public float Force { get; set; } = 10f;
    public float Torque { get; set; } = 100f;
    public int NumberOfSmokePuffs { get; set; } = 4;
    public float SmokePositionVariance { get; set; } = 0.50f;

	void Start ()
    {
        if (gameObjectToMimic == null)
        {
            GameObject.Destroy(this.gameObject);
        }
        else
        {
            Transform mimicTransform = gameObjectToMimic.transform;

            transform.position = mimicTransform.position;
            transform.localScale = mimicTransform.localScale;
            transform.rotation = mimicTransform.rotation;

            MeshRenderer newMeshRenderer = gameObject.AddComponent<MeshRenderer>();
            newMeshRenderer.material = mimicTransform.GetComponent<Renderer>().material;

            MeshFilter newMeshFilter = gameObject.AddComponent<MeshFilter>();
            newMeshFilter.mesh = mimicTransform.GetComponent<MeshFilter>().mesh;

            Vector3 forceVector = new Vector3(Random.Range(-1f, 1f), Random.Range(0.20f, 1f), Random.Range(-1f, 1f));
            forceVector.Normalize();
            forceVector *= Force;

            Rigidbody rigidbody = transform.GetComponent<Rigidbody>();
            rigidbody.AddForce(forceVector, ForceMode.Impulse);

            Vector3 torqueVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            torqueVector.Normalize();
            torqueVector *= Torque;
            rigidbody.AddTorque(torqueVector);

            Color[] smokeColors = new Color[] { Color.black, Color.gray, Color.red };
            for (int i = 0; i < NumberOfSmokePuffs; i++)
            {
                GameObject smoke = Instantiate(smokeObject);
                Vector3 smokePosition = transform.position;
                smokePosition.x += Random.Range(-SmokePositionVariance, SmokePositionVariance);
                smokePosition.y += Random.Range(-SmokePositionVariance, SmokePositionVariance);
                smokePosition.z += Random.Range(-SmokePositionVariance, SmokePositionVariance);
                smoke.transform.position = smokePosition;

                int colorIndex = Random.Range(0, smokeColors.Length);
                smoke.GetComponent<Renderer>().material.color = smokeColors[colorIndex];
            }

            GameObject.Destroy(this.gameObject, Lifetime);
        }
	}

    public void SetObjectToMimic(GameObject gameObject)
    {
        gameObjectToMimic = gameObject;
    }
}
