﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySphere : EnemyBase
{
    private Vector3 scaleNormal;
    private Vector3 eyescaleNormal;  

    private float powerup = 0;
    private float powerupMax = 0.20f;
    private float powerupSpeed = 0.002f;
    private float powerdownSpeed = 0.010f;

    private Transform bodyTransform;
    private Transform eyeTransform;

    private Material normalEyeMaterial;

    [SerializeField]
    private Material angryEyeMaterial;

	public void Start ()
    {
        Transform modelTransform = transform.Find("Model");
        bodyTransform = modelTransform.Find("Body");
        eyeTransform = modelTransform.Find("Eye");
        normalEyeMaterial = eyeTransform.GetComponent<Renderer>().material;

        scaleNormal = new Vector3(bodyTransform.localScale.x, bodyTransform.localScale.y, bodyTransform.localScale.z);
        eyescaleNormal = new Vector3(eyeTransform.localScale.x, eyeTransform.localScale.y, eyeTransform.localScale.z);

        base.Start();
    }

    protected override void AiWanderingHookExecuting()
    {
        RotateTowardsTarget(turnSpeedLazy);
    }

    protected override void AiCheckingHookExecuting()
    {
        if (transform.rotation.eulerAngles == targetDirection)
        {
            GetNewRandomTargetPoint();
        }

        RotateTowardsTarget(turnSpeedLazy);
    }

    protected override void AiPowerupHookBegin()
    {
        SetEyeMaterialToAngry();
    }

    protected override void AiPowerupHookExecuting()
    {
        RotateTowardsTarget(turnSpeedAggro);
        powerup = Mathf.Clamp(powerup + powerupSpeed, 0, powerupMax);
        float powerupHalf = powerup / 2;
        bodyTransform.localScale = scaleNormal + new Vector3(powerup, powerup, powerup);
        //eyeTransform.localScale = eyescaleNormal - new Vector3(powerupHalf, powerupHalf, powerupHalf);
    }

    protected override void AiPowerupHookEnd()
    {
        SetEyeMaterialToNormal();
    }

    protected override void AiPowerdownHookExecuting()
    {
        powerup = Mathf.Clamp(powerup - powerdownSpeed, 0, powerupMax);
        float powerupHalf = powerup / 2;
        bodyTransform.localScale = scaleNormal + new Vector3(powerup, powerup, powerup);
        //eyeTransform.localScale = eyescaleNormal - new Vector3(powerupHalf, powerupHalf, powerupHalf);
    }

    private void SetEyeMaterialToAngry()
    {
        Renderer eyeRenderer = eyeTransform.GetComponent<Renderer>();
        eyeRenderer.material = angryEyeMaterial;
    }

    private void SetEyeMaterialToNormal()
    {
        Renderer eyeRenderer = eyeTransform.GetComponent<Renderer>();
        eyeRenderer.material = normalEyeMaterial;
    }

    protected override void RotateTowardsTarget(float maxSpeed)
    {
        if (transform.forward == targetDirection) return;

        float delta = maxSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), delta);
    }
}
