﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPyramid : EnemyBase
{
    private float turnSpeedHead = 10f;

    private Transform headTrans;

    private Transform eyeTransNorth;
    private Transform eyeTransWest;
    private Transform eyeTransEast;
    private Transform eyeTransSouth;

    private Transform[] eyeTransforms;
    private int currentEye = 0;

    Material normalEyeMaterial;

    [SerializeField]
    Material angryEyeMaterial;

	public void Start ()
    {
        Transform modelTrans = transform.Find("Model");
        headTrans = modelTrans.Find("PyramidHead");
        eyeTransNorth = headTrans.Find("EyeForward");
        eyeTransWest = headTrans.Find("EyeWest");
        eyeTransEast = headTrans.Find("EyeEast");
        eyeTransSouth = headTrans.Find("EyeBackward");

        eyeTransforms = new Transform[] { eyeTransNorth, eyeTransWest, eyeTransSouth, eyeTransEast };

        normalEyeMaterial = eyeTransNorth.GetComponent<Renderer>().material;

        powerupTime = 0.5f;
        cooldownTime = 0.50f;
        timerVariance = 0;

        base.Start();
	}

    protected override void AiPowerupHookBegin()
    {
        SetEyeMaterialToAngry(eyeTransforms[currentEye]);
    }

    protected override void AiPowerupHookExecuting()
    {
        RotateHeadToTarget(turnSpeedHead);
    }

    protected override void AiPowerupHookEnd()
    {
        currentEye = (currentEye + 1) % eyeTransforms.Length;
        SetAllEyeMaterialToNormal();
    }

    protected override void FireBullet()
    {
        Transform currentEyeTrans = eyeTransforms[currentEye];

        GameObject _bullet = Instantiate(bullet);
        _bullet.transform.position = currentEyeTrans.position + currentEyeTrans.forward;
        _bullet.transform.rotation = currentEyeTrans.rotation;
        Rigidbody rbody = _bullet.GetComponent<Rigidbody>();
        rbody.velocity = currentEyeTrans.forward * bulletSpeed;

        _bullet.GetComponent<ShotBehavior>().AddInteractTag("PlayerTarget");

        AudioManager.Instance.PlayClip(audioFireGun);
    }

    private void SetEyeMaterialToAngry(Transform eyeTransform)
    {
        if (angryEyeMaterial == null) return;

        Renderer eyeRenderer = eyeTransform.GetComponent<Renderer>();
        eyeRenderer.material = angryEyeMaterial;
    }

    private void SetAllEyeMaterialToNormal()
    {
        foreach(Transform eyeTransform in eyeTransforms)
        {
            Renderer eyeRenderer = eyeTransform.GetComponent<Renderer>();
            eyeRenderer.material = normalEyeMaterial;
        }
    }

    private void RotateHeadToTarget(float maxSpeed)
    {
        float delta = maxSpeed * Time.deltaTime;

        float angleOffset = (90 * currentEye);

        Quaternion targetRotation = Quaternion.LookRotation(targetDirection, Vector3.up);
        Vector3 newEulers = targetRotation.eulerAngles;
        targetRotation.eulerAngles = new Vector3(0f, newEulers.y + angleOffset, 0f);
        headTrans.rotation = Quaternion.Slerp(headTrans.rotation, targetRotation, delta);
    }
}
