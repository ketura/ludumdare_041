﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArtillery : EnemyBase
{
    private float power = 0f;
    private float maxPower = 0.30f;
    private float powerupSpeed = 0.004f;
    private float powerdownSpeed = 0.025f;

    private float padBaseYScale;

    private Transform padTransform;
    private Transform bulletSpawnHolder;
    private Transform[] bulletSpawns;

    private Material normalPadMaterial;

    [SerializeField]
    public Material angryPadMaterial;

	public void Start ()
    {
        base.Start();

        speed = 0f;
        wanderTime = 0f;
        checkTime = 0f;

        bulletSpawnHolder = transform.Find("BulletSpawns");

        bulletSpawns = new Transform[bulletSpawnHolder.childCount];

        int i = 0;
        foreach (Transform bulletSpawn in bulletSpawnHolder)
        {
            bulletSpawns[i] = bulletSpawn;
            i++;
        }

        Transform modelTransform = transform.Find("Model");
        padTransform = modelTransform.Find("Pad");
        padBaseYScale = padTransform.localScale.y;

        normalPadMaterial = padTransform.GetComponent<Renderer>().material;
	}

    protected override void AiPowerupHookBegin()
    {
        SetPadMaterialToAngry();
    }

    protected override void AiPowerupHookExecuting()
    {
        power = Mathf.Clamp(power + powerupSpeed, 0, maxPower);
        Vector3 scale = padTransform.localScale;
        padTransform.localScale = new Vector3(scale.x, padBaseYScale + power, scale.z);
    }

    protected override void AiPowerupHookEnd()
    {
        SetPadMaterialToNormal();
    }

    protected override void AiPowerdownHookExecuting()
    {
        power = Mathf.Clamp(power - powerdownSpeed, 0, maxPower);
        Vector3 scale = padTransform.localScale;
        padTransform.localScale = new Vector3(scale.x, padBaseYScale + power, scale.z);
    }

    private void SetPadMaterialToNormal()
    {
        padTransform.GetComponent<Renderer>().material = normalPadMaterial;
    }

    private void SetPadMaterialToAngry()
    {
        padTransform.GetComponent<Renderer>().material = angryPadMaterial;
    }

    protected override void FireBullet()
    {
        float randomRotation = Random.Range(0, 360);
        bulletSpawnHolder.Rotate(new Vector3(0f, randomRotation, 0f));

        foreach(Transform bulletTransform in bulletSpawns)
        {
            GameObject _bullet = Instantiate(bullet);
            _bullet.transform.rotation = bulletTransform.rotation;
            _bullet.transform.position = bulletTransform.position;
            Rigidbody rbody = _bullet.GetComponent<Rigidbody>();
            rbody.velocity = bulletTransform.forward * bulletSpeed;

            _bullet.GetComponent<ShotBehavior>().AddInteractTag("PlayerTarget");
        }

        AudioManager.Instance.PlayClip(audioFireGun);
    }
}
