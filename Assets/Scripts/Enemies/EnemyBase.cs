﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBase : MonoBehaviour
{
    public float speed = 3f;
    public float turnSpeedLazy = 100f;
    public float turnSpeedAggro = 270f;

    protected float timer = 0;

    protected float wanderTime = 3f;
    protected float checkTime = 3f;
    protected float powerupTime = 3f;
    protected float cooldownTime = 2f;

    protected float timerVariance = 0.25f;

    public Vector3 targetPoint;
    public Vector3 targetDirection;

    private GameObject[] potentialTargets;
    private GameObject currentTarget;
    public float targetAcquisitionTime = 3f;
    private float targetAcquisitionTimer = 0f;
    public float targetAcquisitionRange = 30f;

    [SerializeField]
    public GameObject bullet;

    [SerializeField]
    protected AudioClip audioFireGun;

    [SerializeField]
    protected AudioClip audioLockOn;

    public float bulletSpeed = 20f;

    protected enum State
    {
        WANDERING,
        CHECKING,
        POWERUP,
        POWERDOWN
    }

    protected State state = State.WANDERING;

    public void Start()
    {
        potentialTargets = GameObject.FindGameObjectsWithTag("PlayerTarget");

        targetPoint = transform.position;
        targetDirection = transform.forward;
        GetNewRandomTargetPoint();
        SetTimer(wanderTime);
        targetAcquisitionTimer = Random.Range(0, targetAcquisitionTime);
    }

    public void Update()
    {
        if (timer > 0) timer -= Time.deltaTime;

        if (targetAcquisitionTimer > 0)
        {
            targetAcquisitionTimer -= Time.deltaTime;
        }
        else
        {
            AcquireTarget();
            targetAcquisitionTimer = targetAcquisitionTime;
        }

        switch (state)
        {
            default:
            case State.WANDERING: AiWander(); break;

            case State.CHECKING: AiChecking(); break;

            case State.POWERUP: AiPowerup(); break;

            case State.POWERDOWN: AiPowerdown(); break;
        }
    }

    private void AiWander()
    {
        MoveTowardsTarget(speed);

        if (currentTarget != null || transform.position == targetPoint || timer <= 0)
        {
            AiWanderingHookEnd();
            SetTimer(checkTime);
            state = State.CHECKING;
        }
        else
        {
            AiWanderingHookExecuting();
        }
    }

    private void AiChecking()
    {
        if (currentTarget != null)
        {
            AiCheckingHookEnd();
            AiPowerupHookBegin();
            SetTimer(powerupTime);
            state = State.POWERUP;
        }
        else if (timer > 0)
        {
            AiCheckingHookExecuting();
        }
        else
        {
            AiCheckingHookEnd();
            GetNewRandomTargetPoint();
            SetTimer(wanderTime);
            state = State.WANDERING;
        }
    }

    private void AiPowerup()
    {
        if (currentTarget != null)
        {
            LookAtTarget();

            if (timer >= 0)
            {
                AiPowerupHookExecuting();
            }
            else
            {
                FireBullet();
                AiPowerupHookEnd();               
                SetTimer(cooldownTime);
                state = State.POWERDOWN;
            }
        }
        else
        {
            AiPowerupHookEnd();
            GetNewRandomTargetPoint();
            SetTimer(wanderTime);
            state = State.WANDERING;
        }
    }

    private void AiPowerdown()
    {
        if (timer >= 0)
        {
            AiPowerdownHookExecuting();
        }
        else
        {
            LookAtTarget();

            if (currentTarget != null)
            {
                AiPowerdownHookEnd();
                AiPowerupHookBegin();
                SetTimer(powerupTime);
                state = State.POWERUP;
            }
            else
            {
                AiPowerdownHookEnd();
                GetNewRandomTargetPoint();
                SetTimer(wanderTime);
                state = State.WANDERING;
            }
        }
    }

    protected void GetNewRandomTargetPoint()
    {
        float moveX = Random.Range(-10f, 10f);
        float moveZ = Random.Range(-10f, 10f);
        targetPoint = targetPoint + new Vector3(moveX, 0f, moveZ);
        targetDirection = targetPoint - transform.position;
        targetDirection.Normalize();
    }

    protected void AcquireTarget()
    {
        GameObject bestPotentialTarget = null;
        float currentShortestDistance = float.MaxValue;

        LD41Utils.ShuffleGameObjects(potentialTargets);

        foreach (GameObject potentialTarget in potentialTargets)
        {
            if (potentialTarget == null) continue;
                
            float distance = Vector3.Distance(transform.position, potentialTarget.transform.position);

            if (distance > targetAcquisitionRange) continue;

            Vector3 direction = potentialTarget.transform.position - transform.position;
            Ray lineOfSightCheck = new Ray(transform.position, direction);
            RaycastHit[] raycastHits = Physics.RaycastAll(lineOfSightCheck, distance);

            bool lineOfSightIsClear = true;
            foreach (RaycastHit hit in raycastHits)
            {
                if (hit.transform.tag == "Solid")
                {
                    lineOfSightIsClear = false;
                    break;
                }
            }

            if (lineOfSightIsClear)
            {
                bestPotentialTarget = potentialTarget;
                currentShortestDistance = distance;
                break;
            }
        }

        if (currentTarget == null && bestPotentialTarget != null)
        {
            AudioManager.Instance.PlayClip(audioLockOn);
        }

        currentTarget = bestPotentialTarget;
    }

    protected void LookAtTarget()
    {
        if (currentTarget != null)
        {
            targetPoint = currentTarget.transform.position;
            targetDirection = targetPoint - transform.position;
            targetDirection.Normalize();
        }
    }

    protected void SetTimer(float time)
    {
        timer = Random.Range(time - timerVariance, time + timerVariance);
    }

    protected virtual void MoveTowardsTarget(float maxSpeed)
    {
        if (transform.position == targetPoint) return;

        float delta = maxSpeed * Time.deltaTime;
        Vector3 translation = Vector3.MoveTowards(transform.position, targetPoint, delta) - transform.position;
        transform.Translate(translation, Space.World);
    }

    protected virtual void RotateTowardsTarget(float maxSpeed)
    {
        float delta = maxSpeed * Time.deltaTime;

        Quaternion newRotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(targetDirection, Vector3.up), delta);
        Vector3 newEulers = newRotation.eulerAngles;
        newRotation.eulerAngles = new Vector3(0f, newEulers.y, 0f);
        transform.rotation = newRotation;
    }

    protected virtual void FireBullet()
    {
        GameObject _bullet = Instantiate(bullet);
        _bullet.transform.rotation = transform.rotation;
        _bullet.transform.position = transform.position + (transform.forward * 2);
        Rigidbody rbody = _bullet.GetComponent<Rigidbody>();
        rbody.velocity = transform.forward * bulletSpeed;

        _bullet.GetComponent<ShotBehavior>().AddInteractTag("PlayerTarget");

        AudioManager.Instance.PlayClip(audioFireGun);
    }

    protected virtual void AiWanderingHookExecuting()
    {
        
    }

    protected virtual void AiWanderingHookEnd()
    {

    }

    protected virtual void AiCheckingHookExecuting()
    {

    }

    protected virtual void AiCheckingHookEnd()
    {

    }

    protected virtual void AiPowerupHookBegin()
    {

    }

    protected virtual void AiPowerupHookExecuting()
    {
        
    }

    protected virtual void AiPowerupHookEnd()
    {

    }

    protected virtual void AiPowerdownHookExecuting()
    {

    }

    protected virtual void AiPowerdownHookEnd()
    {

    }
}
