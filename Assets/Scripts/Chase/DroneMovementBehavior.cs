﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovementBehavior : MovementBehavior
{
    [SerializeField]
    public GameObject MuzzleFlash;

    [SerializeField]
    public GameObject Sparks;

    public KeyCode KeyFire { get; set; } = KeyCode.Space;

    private Transform[] barrels;
    private float muzzleOffset = 0.8f;

	public void Start ()
    {
        KeyForward = KeyCode.UpArrow;
        KeyBackward = KeyCode.DownArrow;
        KeyLeft = KeyCode.LeftArrow;
        KeyRight = KeyCode.RightArrow;

        StrafeEnabled = false;

        Speed = 2;

        int index = 0;
        foreach (Transform child in transform)
        {
            if (child.gameObject.name.Equals("Turret"))
            {
                barrels = new Transform[child.childCount];
                foreach (Transform barrel in child.transform)
                {
                    barrels[index] = barrel;
                    index++;
                }
            }
        }

        base.Start();
	}
	
	public void Update ()
    {
        base.Update();

        if (Input.GetKeyDown(KeyFire))
        {
            foreach(Transform barrelTransform in barrels)
            {
                GameObject muzzleFlash = Instantiate(MuzzleFlash);
                muzzleFlash.transform.position = barrelTransform.position + (transform.forward * muzzleOffset);

                RaycastHit raycastHit;
                if (Physics.Raycast(barrelTransform.position, transform.forward, out raycastHit))
                {
                    GameObject hitFlash = Instantiate(MuzzleFlash);
                    hitFlash.transform.position = raycastHit.point;

                    GameObject sparks = Instantiate(Sparks);
                    sparks.transform.position = raycastHit.point;

                    if (raycastHit.rigidbody != null)
                    {
                        raycastHit.rigidbody.AddExplosionForce(10f, raycastHit.point, 2f);
                    }
                }
            }
        }
	}
}
