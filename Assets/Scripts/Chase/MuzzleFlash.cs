﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour
{
    [SerializeField]
    public float Lifetime = 1f;

	// Use this for initialization
	void Start ()
    {
        Destroy(gameObject, Lifetime);
	}
	
	// Update is called once per frame
	void Update ()
    {

	}
}
