﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehavior : MonoBehaviour
{
    public KeyCode KeyForward { get; set; } = KeyCode.W;
    public KeyCode KeyBackward { get; set; } = KeyCode.S;
    public KeyCode KeyLeft { get; set; } = KeyCode.A;
    public KeyCode KeyRight { get; set; } = KeyCode.D;

    public bool StrafeEnabled { get; set; } = true;
    public KeyCode KeyStrafeLeft { get; set; } = KeyCode.Q;
    public KeyCode KeyStrafeRight { get; set; } = KeyCode.E;

    public float TurnRate { get; set; } = 120;
    public float Speed { get; set; } = 5;

    [SerializeField]
    public GameObject gameCamera;

    // Use this for initialization
    public void Start()
    {
        UpdateCameraPosition();
        UpdateCameraRotation();
    }

    // Update is called once per frame
    public void Update()
    {
        float movementAmount = 0f;
        float turnAmount = 0f;
        float strafeAmount = 0f;

        if (Input.GetKey(KeyForward))
        {
            movementAmount += Speed;
        }

        if (Input.GetKey(KeyBackward))
        {
            movementAmount += -Speed;
        }

        if (StrafeEnabled && Input.GetKey(KeyStrafeLeft))
        {
            strafeAmount -= Speed;
        }

        if (StrafeEnabled && Input.GetKey(KeyStrafeRight))
        {
            strafeAmount += Speed;
        }

        if (Input.GetKey(KeyLeft))
        {
            turnAmount -= TurnRate;
        }

        if (Input.GetKey(KeyRight))
        {
            turnAmount += TurnRate;
        }

        if (turnAmount != 0)
        {
            transform.Rotate(Vector3.up, turnAmount * Time.deltaTime);          
        }

        if (movementAmount != 0)
        {
            transform.position += transform.forward * movementAmount * Time.deltaTime;           
        }

        if (strafeAmount != 0)
        {
            transform.position += transform.right * strafeAmount * Time.deltaTime;
        }

        UpdateCameraRotation();
        UpdateCameraPosition();
    }

    private void UpdateCameraRotation()
    {
        if (gameCamera == null) return;
        gameCamera.transform.rotation = transform.rotation;
    }

    private void UpdateCameraPosition()
    {
        if (gameCamera == null) return;
        gameCamera.transform.position = transform.position;
    }
}
